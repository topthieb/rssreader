package view;

import model.AbstractModel;
import model.Model;
import oberver.Observer;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import controler.AbstractControler;
import controler.ReaderControler;

import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

public class Reader implements Observer {
	private ConnectScreen 		mConnectScreen;
	private ListFeeds			mListFeeds;
	private DisplayFeed			mDisplayFeed;
	private ListAllFeeds		mListAllFeeds;
	private AbstractControler	mControler;
	private MenuItem			mConnecItem, mDecoItem, mEditProfilItem;
	private Image				mImageToolbarFolderAdd, mImageToolbarFeedAdd, mImageToolbarRefresh;
	
	public static void main(String[] args) {
		 //Instanciation de notre modèle
	    AbstractModel model = new Model();
	    //Création du contrôleur
	    AbstractControler controler = new ReaderControler(model);
	    //Création de notre fenêtre avec le contrôleur en paramètre
	    new Reader(controler, model);
	   
	}
	 private Reader(AbstractControler pControler, AbstractModel pModel)
	    {
		 	Display lDisplay = new Display();
		    final Shell lShell = new Shell(lDisplay);
		    lShell.setMinimumSize(new Point(900, 600));
		    final Image lLogo = new Image(lDisplay,
		            "Images/logoRSSbell.png");
		    lShell.setImage(lLogo);

		    /*Center shell*/
		    Monitor primary = lDisplay.getPrimaryMonitor();
		    Rectangle bounds = primary.getBounds();
		    Rectangle rect = lShell.getBounds();
		    
		    int x = bounds.x + (bounds.width - rect.width) / 2;
		    int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		    lShell.setLocation(x, y);
		    lShell.pack();
		    lShell.setLayout(new GridLayout(2, false));
		    
		    //Ajout de la fenêtre comme observer de notre modèle
		    pModel.addObserver(this);
		    
		    /*set member attributes */
		    mControler = pControler;
		    mConnectScreen = new ConnectScreen(mControler, lDisplay);
		    
		    /*Icons toolbar*/
		    mImageToolbarFolderAdd = new Image(lDisplay, "Images/dossier-ajouter-icone-7846-32.png");
		    mImageToolbarFeedAdd = new Image(lDisplay, "Images/add-a-feed-icone-4274-32.png");
		    mImageToolbarRefresh = new Image(lDisplay, "Images/arrow-refresh-icone-4096-32.png");
		    
		    ToolBar toolBar = new ToolBar(lShell, SWT.FLAT | SWT.RIGHT);
		    toolBar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		    
		    ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
		    tltmNewItem.addSelectionListener(new SelectionAdapter() {
		    	@Override
		    	public void widgetSelected(SelectionEvent arg0) {
		    		 if (mControler.getDataUser().isLogged()) {
		    			 FeedPopup lFeedPopup = new FeedPopup(mControler, "Add Feed", lDisplay);
		    			 lFeedPopup.open();
		    		 }
		    	}
		    });
		    tltmNewItem.setImage(mImageToolbarFeedAdd);
		    
		    ToolItem tltmAddFolder = new ToolItem(toolBar, SWT.NONE);
		    tltmAddFolder.addSelectionListener(new SelectionAdapter() {
		    	@Override
		    	public void widgetSelected(SelectionEvent arg0) {
		    		 if (mControler.getDataUser().isLogged()) {
		    			 GenPopup lGenPopup = new GenPopup(mControler, "Add Folder", lDisplay);
		    			 lGenPopup.open();
		    		 }
		    	}
		    });
		    tltmAddFolder.setImage(mImageToolbarFolderAdd);		    
		    
		    ToolItem tltmRefresh = new ToolItem(toolBar, SWT.NONE);
		    tltmRefresh.addSelectionListener(new SelectionAdapter() {
		    	@Override
		    	public void widgetSelected(SelectionEvent arg0) {
		    		if (mControler.getDataUser().isLogged()) 	
		    			mListAllFeeds.updateData(); 		
		    	}
		    });
		    tltmRefresh.setImage(mImageToolbarRefresh);
		    
		    /*set new composites */	
		    mListAllFeeds = new ListAllFeeds(lShell, SWT.BORDER, mControler, lDisplay);
		    mListFeeds = new ListFeeds(lShell, SWT.BORDER, mControler);		
		    mDisplayFeed = new DisplayFeed(lShell, SWT.BORDER, mControler);
		    
		    /*create menu */
		    Menu lMenuBar = new Menu(lShell, SWT.BAR);
		    lShell.setMenuBar(lMenuBar);
		    
		    Menu lFileMenu = new Menu(lMenuBar);
		    Menu lEditMenu = new Menu(lMenuBar);

		    MenuItem lFileItem = new MenuItem(lMenuBar, SWT.CASCADE);
		    lFileItem.setText("File");
		    lFileItem.setMenu(lFileMenu);
		    
		    MenuItem lEditItem = new MenuItem(lMenuBar, SWT.CASCADE);
		    lEditItem.setText("Edit");
		    lEditItem.setMenu(lEditMenu);

		    
		    // Create all the items in the File dropdown menu
		    mConnecItem = new MenuItem(lFileMenu, SWT.NONE);
		    mConnecItem.setText("Connection");
		    mDecoItem = new MenuItem(lFileMenu, SWT.NONE);
		    mDecoItem.setText("Deconnection");
		    
		    // Create all the items in the Edit dropdown menu
		    mEditProfilItem = new MenuItem(lEditMenu, SWT.NONE);
		    mEditProfilItem.setText("Profil");
		    
		    /*add listeners on menu items*/
		    mConnecItem.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {
					// TODO Auto-generated method stub
					mConnectScreen.open();
				}
            	
            });
		    mDecoItem.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {
					// TODO Auto-generated method stub
					mControler.deco();
				}
            	
            });
		    mEditProfilItem.addListener(SWT.Selection, new Listener() {

				@Override
				public void handleEvent(Event event) {
					// TODO Auto-generated method stub
					EditProfilPopup lEditProfilPopup = new EditProfilPopup(mControler, lDisplay);
					lEditProfilPopup.open();
				}
            	
            });
		 
		    if (mControler.getDataUser().isLogged())    {
		    	mConnecItem.setEnabled(false);
		    	mDecoItem.setEnabled(true);
		    	mEditProfilItem.setEnabled(true);
		    }    else {
		    	mConnecItem.setEnabled(true);
		    	mDecoItem.setEnabled(false);
		    	mEditProfilItem.setEnabled(false);
		    }
		    lShell.open();

		    /*if no session */
		    if (true)
		    	mConnectScreen.open();
	        while (!lShell.isDisposed())
	        {
	            if (!lDisplay.readAndDispatch())
	                lDisplay.sleep();
	        }
	        lDisplay.dispose();
	    }
	 void updateMenuSelection()
	 {
		 if (mControler.getDataUser().isLogged())    {
		    	mConnecItem.setEnabled(false);
		    	mDecoItem.setEnabled(true);
		    	mEditProfilItem.setEnabled(true);
		    }    else {
		    	mConnecItem.setEnabled(true);
		    	mDecoItem.setEnabled(false);
		    	mEditProfilItem.setEnabled(false);
		    }
	 }
	@Override
	public void update(String pViewName) {
		updateMenuSelection();
		// TODO Auto-generated method stub
		if (pViewName.contentEquals("ListAllFeed"))
			mListAllFeeds.updateTree();
		else if (pViewName.contentEquals("ListFeeds"))
			mListFeeds.updateList();
		else if (pViewName.contentEquals("All")) {
			mListAllFeeds.updateTree();
			mListFeeds.updateList();
			mDisplayFeed.updateInfo(pViewName);
		}			
		else
			mDisplayFeed.updateInfo(pViewName);
	}
}
