package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;

public class GenPopup {
	 private Shell shell;
	 private CLabel mLabelError;
	 private AbstractControler mControler;
	 private Text text;
	 private Text text_1;

	 public GenPopup(AbstractControler pControler, String pName, Display pDisplay){
		 shell = new Shell(Display.getCurrent());
		 shell.setSize(250, 250);
		 if (pName.contentEquals("Add Folder"))
			 shell.setText(pName + " Page");	
		 else
			 shell.setText("Rename");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shell.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shell.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 shell.setLayout(new GridLayout(2, false));
		 new Label(shell, SWT.NONE);
		 
		 CLabel lblNewLabel_1 = new CLabel(shell, SWT.CENTER);
		 lblNewLabel_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		 lblNewLabel_1.setText(pName);
		 new Label(shell, SWT.NONE);
		 new Label(shell, SWT.NONE);
		 
		  
		 CLabel lblNewLabel = new CLabel(shell, SWT.CENTER);
		 lblNewLabel.setText("Name :");
		 
		 text = new Text(shell, SWT.BORDER);
		 text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		 		
		 if (!pName.contentEquals("Add Folder")) {
			 CLabel lblUrl = new CLabel(shell, SWT.NONE);
			 lblUrl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			 lblUrl.setText("Url :");	
			 text_1 = new Text(shell, SWT.BORDER);
			 text_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			 new Label(shell, SWT.NONE);
		 }
		
		 
		 mLabelError = new CLabel(shell, SWT.NONE);
		 mLabelError.setAlignment(SWT.CENTER);
		 mLabelError.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		 mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 
		 
		 Button btnNewButton = new Button(shell, SWT.NONE);
		 btnNewButton.setText("Validate");
		 new Label(shell, SWT.NONE);
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	if (pName.contentEquals("Add Folder"))
	            		mLabelError.setText(mControler.addFolder(text.getText()));
	            	else
	            		mLabelError.setText(mControler.renameFolderOrLink(pName, text.getText(), text_1.getText()));
	            	if (mLabelError.getText() == "")
	            		shell.close();
	            }
	        });
	
	 }
	 public void open()
    {
        shell.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shell.setVisible(false);
    }
}
