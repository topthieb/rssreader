package view;

import model.RssMessage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import controler.AbstractControler;
import org.eclipse.wb.swt.SWTResourceManager;

public class ListFeeds extends ScrolledComposite {
	private AbstractControler mControler;
	private Table mTable;
	private TableColumn mTableColumnDate, mTableColumnAuthor, mTableColumnTitle;
	
	
	public ListFeeds(Composite arg0, int arg1, AbstractControler pControler) {
		super(arg0, arg1);
		setExpandHorizontal(true);
	    this.setExpandVertical(true);
	    GridData gd_mListFeeds = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
	    gd_mListFeeds.heightHint = 204;
	    this.setLayoutData(gd_mListFeeds);
	   
	    mControler =  pControler;
	    
	    mTable = new Table(this, SWT.BORDER);
	    mTable.setFont(SWTResourceManager.getFont("Sitka Small", 9, SWT.NORMAL));
	    mTable.setLinesVisible(true);
	    mTable.setHeaderVisible(true);
	    
	    mTable.addListener(SWT.Selection, new Listener() {
	        public void handleEvent(Event event) {       
	        	System.out.println(event.item.toString() + " was selected ");
	        	mControler.displayFeed(event.item.toString());
	        }
	      });
	    
	    mTableColumnTitle = new TableColumn(mTable, SWT.NONE);
	    mTableColumnTitle.setWidth(450);
	    mTableColumnTitle.setText("Title");
	    
	    mTableColumnDate = new TableColumn(mTable, SWT.NONE);
	    mTableColumnDate.setWidth(120);
	    mTableColumnDate.setText("Date");
	    
	    mTableColumnAuthor = new TableColumn(mTable, SWT.NONE);
	    mTableColumnAuthor.setWidth(120);
	    mTableColumnAuthor.setText("Author");
	    this.setContent(mTable);
	    this.setMinSize(mTable.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	   
	}
	public void updateList()
	{
		mTable.removeAll();
		if (mControler.getRssMessage() != null)
		for (RssMessage Feed : mControler.getRssMessage())
		{
			TableItem item = new TableItem (mTable, SWT.NONE);
			item.setText (new String[] { Feed.getTitle(), Feed.getDate(), Feed.getAuthor() == null ? "No Author" : Feed.getAuthor()});
		}
	}
}
