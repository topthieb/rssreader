package view;

import model.RssFolder;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;

public class FeedPopup {
	 private Shell shell;
	 private CLabel mLabelError;
	 private AbstractControler mControler;
	 private Text text;
	 private Text text_1;
	 private Combo combo;
	 private CLabel lblFolder;

	 public FeedPopup(AbstractControler pControler, String pName, Display pDisplay){
		 shell = new Shell(Display.getCurrent());
		 shell.setSize(250, 250);
		 shell.setText(pName + " Page");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shell.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shell.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 shell.setLayout(new GridLayout(2, false));
		 new Label(shell, SWT.NONE);
		 
		 CLabel lblNewLabel_1 = new CLabel(shell, SWT.CENTER);
		 lblNewLabel_1.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		 lblNewLabel_1.setText(pName);
		 
		 lblFolder = new CLabel(shell, SWT.NONE);
		 lblFolder.setText("Folder :");
		 
		 combo = new Combo(shell, SWT.NONE);
		 combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		 String [] lItemsFolder = new String[mControler.getRssFolder().size()];
		 int i = 0;
		 for (RssFolder lRssFolder : mControler.getRssFolder()) {
			 lItemsFolder[i] = lRssFolder.getTitle();
			 i++;
		 }
		 combo.setItems(lItemsFolder);
		  
		 CLabel lblNewLabel = new CLabel(shell, SWT.CENTER);
		 lblNewLabel.setText("Name :");
		 
		 text = new Text(shell, SWT.BORDER);
		 text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		 		
		 if (!pName.contentEquals("Add Folder")) {
			 CLabel lblUrl = new CLabel(shell, SWT.NONE);
			 lblUrl.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			 lblUrl.setText("Url :");	
			 text_1 = new Text(shell, SWT.BORDER);
			 text_1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			 new Label(shell, SWT.NONE);
		 }
		
		 mLabelError = new CLabel(shell, SWT.CENTER);
		 mLabelError.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		 mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 
		 
		 Button btnNewButton = new Button(shell, SWT.NONE);
		 btnNewButton.setText("Validate");
		 new Label(shell, SWT.NONE);
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	mLabelError.setText(mControler.addFeed(combo.getText(), text.getText(), text_1.getText()));
	            	if (mLabelError.getText() == "")
	            		shell.close();
	            }
	        });
	
	 }
	 public void open()
    {
        shell.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shell.setVisible(false);
    }
}
