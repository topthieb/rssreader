package view;

import model.RssMessage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import controler.AbstractControler;
import org.eclipse.wb.swt.SWTResourceManager;

public class DisplayFeed extends ScrolledComposite {
	private AbstractControler 	mControler;
	private Text				mTitle;
	private Browser				mBrowser;
	
	public DisplayFeed(Composite arg0, int arg1, AbstractControler pControler) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
		
	    this.setExpandHorizontal(true);
	    this.setExpandVertical(true);
	    
	    mControler = pControler;
	    GridData gd_mDisplayFeed = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
	    gd_mDisplayFeed.heightHint = 271;
	    this.setLayoutData(gd_mDisplayFeed);	
	   	    
	    SashForm sashForm = new SashForm(this, SWT.VERTICAL);
	    sashForm.setFont(SWTResourceManager.getFont("Sitka Small", 9, SWT.NORMAL));
	    
	    mTitle = new Text(sashForm, SWT.BORDER | SWT.READ_ONLY | SWT.MULTI | SWT.CENTER);
	    mTitle.setFont(SWTResourceManager.getFont("Sitka Small", 9, SWT.NORMAL));
	    
	    mBrowser = new Browser(sashForm, SWT.NONE);
	    mBrowser.setFont(SWTResourceManager.getFont("Sitka Small", 9, SWT.NORMAL));
	    sashForm.setWeights(new int[] {43, 265});
	    this.setContent(sashForm);
	}
	
	public void updateInfo(String lTitle)
	{
		mTitle.setText("");
		mBrowser.setText("");
		if (lTitle != "" && lTitle != null && mControler.getSelectedFeed(lTitle) != null) {
			RssMessage lFeed = mControler.getSelectedFeed(lTitle);
			if (lFeed != null) {
				mTitle.setText(lFeed.getTitle());
				mBrowser.setText(lFeed.getContent());
			}
		}
	}
}
