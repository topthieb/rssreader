package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

public class ForgetPasswordPopup {

	 private Shell shell;
	 private CLabel mLabelError;
	 private AbstractControler mControler;
	 private Text text;
	 private CLabel lblForgetPassword;

	 public ForgetPasswordPopup(AbstractControler pControler, Display pDisplay){
		 shell = new Shell(Display.getCurrent());
		 shell.setSize(250, 250);
		 shell.setText(" Forget Password");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shell.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shell.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 shell.setLayout(new GridLayout(2, false));
			 
		 lblForgetPassword = new CLabel(shell, SWT.CENTER);
		 lblForgetPassword.setAlignment(SWT.CENTER);
		 GridData gd_lblForgetPassword = new GridData(SWT.CENTER, SWT.FILL, false, false, 2, 1);
		 gd_lblForgetPassword.heightHint = 81;
		 lblForgetPassword.setLayoutData(gd_lblForgetPassword);
		 lblForgetPassword.setText("Forget Password");

		 
		 CLabel lblNewLabel = new CLabel(shell, SWT.CENTER);
		 lblNewLabel.setText("Email :");
		  
		  text = new Text(shell, SWT.BORDER);
		  GridData gd_text = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		  gd_text.widthHint = 151;
		  text.setLayoutData(gd_text);
		 
		  mLabelError = new CLabel(shell, SWT.CENTER);
		  mLabelError.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		  mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 
		 
		 Button btnNewButton = new Button(shell, SWT.NONE);
		 btnNewButton.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		 btnNewButton.setText("Validate");
		 new Label(shell, SWT.NONE);
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	mLabelError.setText(mControler.recupPassword(text.getText()));
	            	if (mLabelError.getText() == "")
	            		shell.close();
	            }
	        });
	
	 }
	 public void open()
    {
        shell.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shell.setVisible(false);
    }
}
