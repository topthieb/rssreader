package view;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import model.RssFolder;
import model.RssLink;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

public class ListAllFeeds extends ScrolledComposite {
	private Tree		mTree ;
	private AbstractControler mControler;
	private Image		mImageFolder;
	private Display		mDisplay;
	/**
	 * @wbp.parser.entryPoint
	 */
	public ListAllFeeds(Composite arg0, int arg1, AbstractControler pControler, Display lDisplay) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	    this.setExpandHorizontal(true);
	    this.setExpandVertical(true);
	      
	    mDisplay = lDisplay;
	    mControler = pControler;
	    mImageFolder = new Image(lDisplay, "Images/dossier-de-lalimentation-icone-8119-16.png");
	    mTree = new Tree(this, SWT.BORDER | SWT.V_SCROLL
	            | SWT.H_SCROLL | SWT.SINGLE);
	    mTree.setFont(SWTResourceManager.getFont("Sitka Small", 9, SWT.NORMAL));
	    this.setContent(mTree);
	    this.setMinSize(mTree.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	    GridData gd_mListAllFeeds = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 2);
	    gd_mListAllFeeds.widthHint = 150;
	    this.setLayoutData(gd_mListAllFeeds);
	    
	    mTree.addListener(SWT.Selection, new Listener() {
	        public void handleEvent(Event event) {       
	        	System.out.println(event.item.toString() + " was selected ");
	        	mControler.showFeed(event.item.toString());
	        }
	      });
	  
	    final Menu menu = new Menu(mTree);
	    mTree.setMenu(menu);
	    menu.addMenuListener(new MenuAdapter()
	    {
	        public void menuShown(MenuEvent e)
	        {
	            MenuItem[] items = menu.getItems();
	            for (int i = 0; i < items.length; i++)
	            {
	                items[i].dispose();
	            }
	            MenuItem lItemDelete = new MenuItem(menu, SWT.NONE);
	            lItemDelete.setText("Delete " + mTree.getSelection()[0].getText());
	            lItemDelete.addListener(SWT.Selection, new Listener() {

					@Override
					public void handleEvent(Event event) {
						// TODO Auto-generated method stub
						mControler.deleteRssFolderOrLink(mTree.getSelection()[0].getText());
					}
	            	
	            });
	            MenuItem lItemRename = new MenuItem(menu, SWT.NONE);
	            lItemRename.setText("Rename " + mTree.getSelection()[0].getText());
	            lItemRename.addListener(SWT.Selection, new Listener() {

					@Override
					public void handleEvent(Event event) {
						// TODO Auto-generated method stub
						GenPopup lGenPopup = new GenPopup(mControler, mTree.getSelection()[0].getText(), lDisplay);
						lGenPopup.open();
					}
	            	
	            });
	        }
	    });
	}
	
	public void updateData()
	{
		mControler.getListFolder();
	}
	private  URL getImageURL(String pUrl) {
        URL tmpURL;
        try {
            tmpURL = new URL(pUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
        // in the end? let's turn back on start
        return tmpURL;
    }

	public void updateTree()
	{
		Image lIcon = null;
		mTree.removeAll();
		if (mControler.getRssFolder() != null)
			for (RssFolder Folder : mControler.getRssFolder()) {
				TreeItem lTreeItem = new TreeItem(mTree, SWT.NONE);
				lTreeItem.setText(Folder.getTitle());
				lTreeItem.setImage(mImageFolder);
				for (RssLink Link : Folder.getLinks()) {
					TreeItem lTreeItem1 = new TreeItem(lTreeItem, SWT.NONE);
					lTreeItem1.setText(Link.getTitle());
					try {						
						InputStream lUrl = getImageURL(Link.getIcon()).openStream();						
						lIcon = new Image(mDisplay, lUrl);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						lIcon = new Image(mDisplay, "Images/logo-64-64-black-and-white.png");
					} catch (org.eclipse.swt.SWTException e)	{
						lIcon = new Image(mDisplay, "Images/logo-64-64-black-and-white.png");
					}
					if (lIcon != null)
						lTreeItem1.setImage(lIcon);
				}
			}
	}
}
