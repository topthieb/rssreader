package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

public class ConnectScreen {
	 private Shell shell;
	 private Text mLogin;
	 private Text mPassword;
	 private CLabel mLabelError;
	 private AbstractControler mControler;

	 public ConnectScreen(AbstractControler pControler, Display pDisplay){
		 shell = new Shell(Display.getCurrent(), SWT.TITLE);
		 shell.setSize(400, 400);
		 shell.setLayout(new FormLayout());
		 shell.setText("Connection Page");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shell.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shell.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 
		 CLabel lblConnectionToYour = new CLabel(shell, SWT.NONE);
		 lblConnectionToYour.setAlignment(SWT.CENTER);
		 FormData fd_lblConnectionToYour = new FormData();
		 fd_lblConnectionToYour.top = new FormAttachment(0, 10);
		 fd_lblConnectionToYour.left = new FormAttachment(0, 88);
		 lblConnectionToYour.setLayoutData(fd_lblConnectionToYour);
		
		 lblConnectionToYour.setImage(new Image(pDisplay, "Images/logoRSSbell (1).png"));
		
		 
		 CLabel lblNewLabel = new CLabel(shell, SWT.NONE);
		 FormData fd_lblNewLabel = new FormData();
		 fd_lblNewLabel.left = new FormAttachment(0, 1);
		 lblNewLabel.setLayoutData(fd_lblNewLabel);
		 lblNewLabel.setText("Username :");
		 
		 mLogin = new Text(shell, SWT.BORDER);
		 fd_lblConnectionToYour.bottom = new FormAttachment(mLogin, -19);
		 fd_lblConnectionToYour.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 FormData fd_text = new FormData();
		 fd_text.right = new FormAttachment(100, -84);
		 fd_text.left = new FormAttachment(lblNewLabel, 62);
		 fd_text.bottom = new FormAttachment(lblNewLabel, 0, SWT.BOTTOM);
		 mLogin.setLayoutData(fd_text);
	
		 
		 CLabel lblNewLabel_1 = new CLabel(shell, SWT.NONE);
		 fd_lblNewLabel.bottom = new FormAttachment(lblNewLabel_1, -18);
		 FormData fd_lblNewLabel_1 = new FormData();
		 fd_lblNewLabel_1.top = new FormAttachment(0, 218);
		 fd_lblNewLabel_1.left = new FormAttachment(0, 1);
		 lblNewLabel_1.setLayoutData(fd_lblNewLabel_1);
		 lblNewLabel_1.setText("Password :");
		 
		 mPassword = new Text(shell,SWT.PASSWORD | SWT.BORDER);
		 FormData fd_text_1 = new FormData();
		 fd_text_1.right = new FormAttachment(100, -84);
		 fd_text_1.left = new FormAttachment(lblNewLabel_1, 65);
		 fd_text_1.bottom = new FormAttachment(lblNewLabel_1, 0, SWT.BOTTOM);
		 mPassword.setLayoutData(fd_text_1);
		 
		 
		 Button btnNewButton = new Button(shell, SWT.NONE);
		 FormData fd_btnNewButton = new FormData();
		 fd_btnNewButton.bottom = new FormAttachment(100, -59);
		 fd_btnNewButton.right = new FormAttachment(100, -10);
		 btnNewButton.setLayoutData(fd_btnNewButton);
		 btnNewButton.setText("Login");
		 btnNewButton.setImage(new Image(pDisplay, "Images/accepter-check-ok-oui-icone-4851-32.png"));
		 
		 mLabelError = new CLabel(shell, SWT.NONE);
		 mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 FormData fd_lblOr = new FormData();
		 fd_lblOr.top = new FormAttachment(mPassword, 1);
		 fd_lblOr.left = new FormAttachment(0, 128);
		 fd_lblOr.right = new FormAttachment(100, -84);
		 mLabelError.setLayoutData(fd_lblOr);
		 
		 Button btnNewButton_1 = new Button(shell, SWT.NONE);
		 FormData fd_btnNewButton_1 = new FormData();
		 fd_btnNewButton_1.top = new FormAttachment(btnNewButton, 0, SWT.TOP);
		 fd_btnNewButton_1.right = new FormAttachment(btnNewButton, -17);
		 btnNewButton_1.setLayoutData(fd_btnNewButton_1);
		 btnNewButton_1.setText("Forget password ?");
		 btnNewButton_1.setImage(new Image(pDisplay, "Images/aider-icone-6740-32.png"));
		 
		 Button btnNewButton_2 = new Button(shell, SWT.NONE);
		 FormData fd_btnNewButton_2 = new FormData();
		 fd_btnNewButton_2.top = new FormAttachment(btnNewButton, 0, SWT.TOP);
		 fd_btnNewButton_2.left = new FormAttachment(0, 10);
		 btnNewButton_2.setLayoutData(fd_btnNewButton_2);
		 btnNewButton_2.setText("Register");
		 btnNewButton_2.setImage(new Image(pDisplay, "Images/user-add-icone-8041-32.png"));
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	String lResponse = mControler.connect(mLogin.getText(), mPassword.getText());	            	
	            	if (lResponse != "")
	            		mLabelError.setText(lResponse);
	            	else
	            		close();
	            }
	        });
		 btnNewButton_1.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	ForgetPasswordPopup lForgetPasswordPopup =  new ForgetPasswordPopup(pControler, pDisplay);
	            	lForgetPasswordPopup.open();
	            }
	        });	
		 btnNewButton_2.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	CreateAccountPopup lCreateAccountPopup =  new CreateAccountPopup(pControler, pDisplay);
	            	lCreateAccountPopup.open();
	            }
	        });
	
	 }
	 public void open()
    {
        shell.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shell.setVisible(false);
    }
}
