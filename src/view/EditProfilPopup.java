package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

public class EditProfilPopup {
	 private Shell shlEditProfil;
	 private Text mLogin;
	 private Text mPassword;
	 private CLabel mLabelError;
	 private AbstractControler mControler;

	 public EditProfilPopup(AbstractControler pControler, Display pDisplay){
		 shlEditProfil = new Shell(Display.getCurrent());
		 shlEditProfil.setSize(400, 400);
		 shlEditProfil.setLayout(new FormLayout());
		 shlEditProfil.setText("Edit Profil");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shlEditProfil.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shlEditProfil.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 
		 CLabel lblConnectionToYour = new CLabel(shlEditProfil, SWT.NONE);
		 lblConnectionToYour.setAlignment(SWT.CENTER);
		 FormData fd_lblConnectionToYour = new FormData();
		 fd_lblConnectionToYour.top = new FormAttachment(0, 38);
		 lblConnectionToYour.setLayoutData(fd_lblConnectionToYour);
		 lblConnectionToYour.setText("Edit your informations");
		
		 
		 CLabel lblNewLabel = new CLabel(shlEditProfil, SWT.NONE);
		 FormData fd_lblNewLabel = new FormData();
		 fd_lblNewLabel.left = new FormAttachment(0, 16);
		 lblNewLabel.setLayoutData(fd_lblNewLabel);
		 lblNewLabel.setText("Username :");
		 
		 mLogin = new Text(shlEditProfil, SWT.BORDER);
		 fd_lblNewLabel.top = new FormAttachment(mLogin, 0, SWT.TOP);
		 fd_lblConnectionToYour.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 fd_lblConnectionToYour.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 FormData fd_text = new FormData();
		 fd_text.right = new FormAttachment(100, -84);
		 fd_text.left = new FormAttachment(lblNewLabel, 41);
		 mLogin.setLayoutData(fd_text);
	
		 
		 CLabel lblNewLabel_1 = new CLabel(shlEditProfil, SWT.NONE);
		 FormData fd_lblNewLabel_1 = new FormData();
		 fd_lblNewLabel_1.top = new FormAttachment(lblNewLabel, 20);
		 fd_lblNewLabel_1.left = new FormAttachment(lblNewLabel, 0, SWT.LEFT);
		 lblNewLabel_1.setLayoutData(fd_lblNewLabel_1);
		 lblNewLabel_1.setText("Password :");
		 
		 mPassword = new Text(shlEditProfil,SWT.PASSWORD | SWT.BORDER);
		 fd_text.bottom = new FormAttachment(mPassword, -20);
		 FormData fd_text_1 = new FormData();
		 fd_text_1.right = new FormAttachment(lblConnectionToYour, 0, SWT.RIGHT);
		 fd_text_1.left = new FormAttachment(lblConnectionToYour, 0, SWT.LEFT);
		 fd_text_1.top = new FormAttachment(0, 179);
		 mPassword.setLayoutData(fd_text_1);
		 
		 
		 Button btnNewButton = new Button(shlEditProfil, SWT.NONE);
		 FormData fd_btnNewButton = new FormData();
		 fd_btnNewButton.right = new FormAttachment(lblConnectionToYour, 0, SWT.RIGHT);
		 fd_btnNewButton.left = new FormAttachment(0, 209);
		 btnNewButton.setLayoutData(fd_btnNewButton);
		 btnNewButton.setText("Save");
		 
		 mLabelError = new CLabel(shlEditProfil, SWT.NONE);
		 fd_btnNewButton.top = new FormAttachment(mLabelError, 6);
		 mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 FormData fd_label = new FormData();
		 fd_label.top = new FormAttachment(mPassword, 40);
		 fd_label.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 fd_label.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 mLabelError.setLayoutData(fd_label);
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	String lResponse = mControler.updateInfoUser(mLogin.getText(), mPassword.getText());	            	
	            	if (lResponse != "")
	            		mLabelError.setText(lResponse);
	            	else
	            		close();
	            }
	        });
	
	 }
	 public void open()
    {
        shlEditProfil.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shlEditProfil.setVisible(false);
    }
}