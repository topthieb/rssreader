package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import controler.AbstractControler;

public class CreateAccountPopup {
	 private Shell shell;
	 private Text mLogin;
	 private Text mPassword;
	 private CLabel mLabelError;
	 private AbstractControler mControler;
	 private Text mEmail;
	 private Text mConfirmPassword;
	 private CLabel lblEmail;
	 private CLabel lblPassword;

	 public CreateAccountPopup(AbstractControler pControler, Display pDisplay){
		 shell = new Shell(Display.getCurrent());
		 shell.setSize(400, 400);
		 shell.setLayout(new FormLayout());
		 shell.setText("Create Account Page");
		 
		 /* Center Shell */
		 Monitor primary = pDisplay.getPrimaryMonitor();
		 Rectangle bounds = primary.getBounds();
		 Rectangle rect = shell.getBounds();
		    
		 int x = bounds.x + (bounds.width - rect.width) / 2;
		 int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		 shell.setLocation(x, y);
		 
		 /*set member attributes*/
		 mControler = pControler;
		 
		 CLabel lblConnectionToYour = new CLabel(shell, SWT.NONE);
		 lblConnectionToYour.setAlignment(SWT.CENTER);
		 FormData fd_lblConnectionToYour = new FormData();
		 fd_lblConnectionToYour.top = new FormAttachment(0, 40);
		 fd_lblConnectionToYour.right = new FormAttachment(100, -137);
		 fd_lblConnectionToYour.left = new FormAttachment(0, 150);
		 lblConnectionToYour.setLayoutData(fd_lblConnectionToYour);
		 lblConnectionToYour.setText("Get an account");
		
		 
		 CLabel lblNewLabel = new CLabel(shell, SWT.NONE);
		 FormData fd_lblNewLabel = new FormData();
		 lblNewLabel.setLayoutData(fd_lblNewLabel);
		 lblNewLabel.setText("Username :");
		 
		 mLogin = new Text(shell, SWT.BORDER);
		 FormData fd_text = new FormData();
		 fd_text.right = new FormAttachment(100, -84);
		 fd_text.left = new FormAttachment(lblNewLabel, 62);
		 fd_text.top = new FormAttachment(lblNewLabel, 0, SWT.TOP);
		 mLogin.setLayoutData(fd_text);
	
		 
		 CLabel lblNewLabel_1 = new CLabel(shell, SWT.NONE);
		 fd_lblNewLabel.bottom = new FormAttachment(lblNewLabel_1, -20);
		 fd_lblNewLabel.left = new FormAttachment(lblNewLabel_1, 0, SWT.LEFT);
		 FormData fd_lblNewLabel_1 = new FormData();
		 fd_lblNewLabel_1.bottom = new FormAttachment(100, -171);
		 fd_lblNewLabel_1.left = new FormAttachment(0, 1);
		 lblNewLabel_1.setLayoutData(fd_lblNewLabel_1);
		 lblNewLabel_1.setText("Password :");
		 
		 mPassword = new Text(shell,SWT.PASSWORD | SWT.BORDER);
		 FormData fd_text_1 = new FormData();
		 fd_text_1.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 fd_text_1.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 fd_text_1.top = new FormAttachment(lblNewLabel_1, 0, SWT.TOP);
		 mPassword.setLayoutData(fd_text_1);
		 
		 
		 Button btnNewButton = new Button(shell, SWT.NONE);
		 FormData fd_btnNewButton = new FormData();
		 fd_btnNewButton.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 btnNewButton.setLayoutData(fd_btnNewButton);
		 btnNewButton.setText("Create");
		 
		 mLabelError = new CLabel(shell, SWT.NONE);
		 fd_btnNewButton.top = new FormAttachment(0, 296);
		 mLabelError.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		 FormData fd_lblOr = new FormData();
		 fd_lblOr.bottom = new FormAttachment(btnNewButton, -16);
		 fd_lblOr.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 fd_lblOr.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 mLabelError.setLayoutData(fd_lblOr);
		 
		 mEmail = new Text(shell, SWT.BORDER);
		 FormData fd_text1 = new FormData();
		 fd_text1.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 fd_text1.bottom = new FormAttachment(mLogin, -17);
		 fd_text1.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 mEmail.setLayoutData(fd_text1);
		 
		 mConfirmPassword = new Text(shell, SWT.BORDER | SWT.PASSWORD);
		 FormData fd_text_11 = new FormData();
		 fd_text_11.bottom = new FormAttachment(mLabelError, -19);
		 fd_text_11.left = new FormAttachment(mLogin, 0, SWT.LEFT);
		 fd_text_11.right = new FormAttachment(mLogin, 0, SWT.RIGHT);
		 mConfirmPassword.setLayoutData(fd_text_11);
		 
		 lblEmail = new CLabel(shell, SWT.NONE);
		 FormData fd_lblEmail = new FormData();
		 fd_lblEmail.top = new FormAttachment(mEmail, 0, SWT.TOP);
		 fd_lblEmail.left = new FormAttachment(lblNewLabel, 0, SWT.LEFT);
		 lblEmail.setLayoutData(fd_lblEmail);
		 lblEmail.setText("Email :");
		 
		 lblPassword = new CLabel(shell, SWT.NONE);
		 FormData fd_lblPassword = new FormData();
		 fd_lblPassword.bottom = new FormAttachment(mConfirmPassword, 0, SWT.BOTTOM);
		 fd_lblPassword.left = new FormAttachment(lblNewLabel, 0, SWT.LEFT);
		 lblPassword.setLayoutData(fd_lblPassword);
		 lblPassword.setText("Confirm Password :");
		 
		 btnNewButton.addSelectionListener(new SelectionAdapter()
	        {
	            @Override public void widgetSelected(final SelectionEvent e)
	            {
	            	if (!mPassword.getText().contentEquals(mConfirmPassword.getText())) {
	            		mLabelError.setText("Password not matching");
	            		return ;
	            	}
	            	String lResponse = mControler.createAccount(mEmail.getText(), mLogin.getText(), mPassword.getText());	            	
	            	if (lResponse != "")
	            		mLabelError.setText(lResponse);
	            	else
	            		close();
	            }
	        });
	
	 }
	 public void open()
    {
        shell.open();
    }

    public void close()
    {
       // Don't call shell.close(), because then
       // you'll have to re-create it
        shell.setVisible(false);
    }
}
