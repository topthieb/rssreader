package model;

import java.util.ArrayList;

/**
 * Created by ROMMM on 09/10/14.
 */
public class RssMessage {

    private String title;
    private String link;
    private String author;
    private String date;
    private String contentSnippet;
    private String content;
    private ArrayList<String> imgUrls;
    private ArrayList<String> categories;

    public RssMessage(String title) {
        this.title = title;
        categories = new ArrayList<String>();
        imgUrls = new ArrayList<String>();
    }

    public RssMessage() {
        categories = new ArrayList<String>();
        imgUrls = new ArrayList<String>();
    }

    public ArrayList<String> getImgUrls() {
        return imgUrls;
    }

    public void setImgUrls(ArrayList<String> imgUrls) {
        this.imgUrls = imgUrls;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContentSnippet() {
        return contentSnippet;
    }

    public void setContentSnippet(String contentSnippet) {
        this.contentSnippet = contentSnippet;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }
}