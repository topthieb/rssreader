package model;

import java.util.ArrayList;

import oberver.Observable;
import oberver.Observer;


public abstract class AbstractModel implements Observable{

  private ArrayList<Observer> listObserver = new ArrayList<Observer>();  
  
  abstract public void connect(String pViewName);
  abstract public void setUser(User pUser);
  abstract public User getUser();
  abstract public void setRssFolder(ArrayList<RssFolder> pRssFolder);
  abstract public void setDataUser(String pLogin, String pPassword);
  abstract public DataUser getDataUser();
  abstract public ArrayList<RssFolder> getRssFolder();
  abstract public ArrayList<RssMessage> getRssMessage();
  abstract public void removeLink(RssFolder pRssFolder, RssLink pLink);
  abstract public void removeRssFolder(RssFolder pRssFolder);
  abstract public void setRssMessage(ArrayList<RssMessage> pRssMessage);
  abstract public void addRssFolder(RssFolder pRssFolder);
  abstract public void addRssLink(RssFolder pRssFolder, RssLink pRssLink);
  
  //Implémentation du pattern observer
  public void addObserver(Observer obs) {
    this.listObserver.add(obs);
  }

  public void notifyObserver(String pViewName) {
    for(Observer obs : listObserver)
      obs.update(pViewName);
  }

  public void removeObserver() {
    listObserver = new ArrayList<Observer>();
  } 
}