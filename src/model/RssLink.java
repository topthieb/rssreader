package model;

public class RssLink {
    private String mFeedId;
    private String mTitle;
    private String mUrl;
    private	String mIcon;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }
    
    public void setIcon(String pIcon) {
    	this.mIcon = pIcon;
    }
    public String getIcon() {
    	return mIcon;
    }

    public String getFeedId() {
        return mFeedId;
    }

    public void setFeedId(String mFeedId) {
        this.mFeedId = mFeedId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}