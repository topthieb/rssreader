package model;

import java.util.ArrayList;

public class Model extends AbstractModel{
	  protected User					mUser = null;
	  protected DataUser				mDataUser = null;
	  protected ArrayList<RssFolder>	mRssFolder = null;
	  protected ArrayList<RssMessage>	mRssMessage = null;
	  
	  
	  public void setUser(User pUser)
	   {
		   mUser = pUser;
	   }
	  public User getUser()
	  {
		  return mUser;
	  }
	   public void setRssFolder(ArrayList<RssFolder> pRssFolder)
	   {
		   mRssFolder = pRssFolder;
	   }
	   public void setDataUser(String pLogin, String pPassword)
	   {
		   mDataUser = new DataUser(pLogin, pPassword);
	   }
	   public DataUser getDataUser() {
		   return mDataUser;
	   }
	   public ArrayList<RssFolder> getRssFolder()
	   {
		   return mRssFolder;
	   }
	   public void removeLink(RssFolder pRssFolder, RssLink pLink)
	   {
		   pRssFolder.getLinks().remove(pLink);
	   }
	   public void removeRssFolder(RssFolder pRssFolder) {
		   mRssFolder.remove(pRssFolder);
	   }
	   public void connect(String pViewName){
		  notifyObserver(pViewName);
	  }
	   public void addRssLink(RssFolder pRssFolder, RssLink pRssLink)
	   {
		   pRssFolder.getLinks().add(pRssLink);
	   }
	   public void setRssMessage(ArrayList<RssMessage> pRssMessage)
	   {
		   mRssMessage = pRssMessage;
	   }
	   public ArrayList<RssMessage> getRssMessage() {
		   return mRssMessage;
	   }
	   public void addRssFolder(RssFolder pRssFolder)
	   {
		   mRssFolder.add(pRssFolder);
	   }
}