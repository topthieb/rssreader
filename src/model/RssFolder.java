package model;

import java.util.ArrayList;

/**
 * Created by ROMMM on 22/10/14.
 */
public class RssFolder {

    private String mTitle;
    private String mFolderId;
    private ArrayList<RssLink> mLinks;

    public RssFolder() { }

    public RssFolder (String title, ArrayList<RssLink> links) {
        mTitle = title;
        mLinks = links;
    }

    public ArrayList<RssLink> getLinks() {
        return mLinks;
    }

    public void setLinks(ArrayList<RssLink> mLinks) {
        this.mLinks = mLinks;
    }

    public String getId() {
        return mFolderId;
    }

    public void setId(String id) {
        this.mFolderId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
}