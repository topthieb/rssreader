package model;

public class DataUser {
	private String mLogin, mPassword = null;
	
	public DataUser(String pLogin, String pPassword)
	{
		mLogin = pLogin;
		mPassword = pPassword;
	}

	public String getmLogin() {
		return mLogin;
	}

	public void setmLogin(String mLogin) {
		this.mLogin = mLogin;
	}

	public String getmPassword() {
		return mPassword;
	}

	public void setmPassword(String mPassword) {
		this.mPassword = mPassword;
	}
	public boolean isLogged()
	{
		return mLogin == null ? false : true;
	}
}
