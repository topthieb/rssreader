package controler;

import java.util.ArrayList;

import parser.JsonParser;
import Requests.ApiUrls;
import Requests.MyHttpURLConnection;
import model.AbstractModel;
import model.DataUser;
import model.RssFolder;
import model.RssLink;
import model.RssMessage;
import model.User;

public abstract class AbstractControler {
	  protected AbstractModel 			mModel;
	  protected MyHttpURLConnection	 	mUrl;
	
	  
	  public AbstractControler(AbstractModel pModel){
		  mModel = pModel;
		  mUrl = new MyHttpURLConnection();
	   }

	  public String getHeader()
	  {
		 return getDataUser().getmLogin() + ":" + getDataUser().getmPassword();
	  }
	  public MyHttpURLConnection getUrl()
	  {
		  return mUrl;
	  }
	  public void deco()
	  {
		  String lResponse = null;
      		try {
      			lResponse = mUrl.sendGet(ApiUrls.RSS_LOGOUT, getHeader());
	       	} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();				
				}
	       	if (lResponse != null)
	       	{
	       		mModel.setDataUser(null, null);
				mModel.setRssFolder(null);
				mModel.setRssMessage(null);
				mModel.setUser(null);
				control("All");
	       	}		 
	  }
	   public void setUser(User pUser)
	   {
		   mModel.setUser(pUser);
		   control("ListAllFeed");
	   }
	   public User getUser()
	   {
		   return mModel.getUser();
	   }
	   public void setRssFolder(ArrayList<RssFolder> pRssFolder)
	   {
		   mModel.setRssFolder(pRssFolder);
		   control("ListAllFeed");
	   }
	   public void addRssFolder(RssFolder pRssFolder)
	   {
		   mModel.addRssFolder(pRssFolder);
		   control("ListAllFeed");
	   }
	   public void setRssMessage(ArrayList<RssMessage> pRssMessage)
	   {
		   mModel.setRssMessage(pRssMessage);
		   control("ListFeeds");
	   }
	   public void setDataUser(String pLogin, String pPassword)
	   {
		   mModel.setDataUser(pLogin, pPassword);
	   }
	   public DataUser getDataUser() {
		   return mModel.getDataUser() == null ? new DataUser(null, null) :  mModel.getDataUser();
	   }
	   public ArrayList<RssFolder> getRssFolder()
	   {
		   return mModel.getRssFolder();
	   }
	   public void removeLink(RssFolder pRssFolder, RssLink pLink)
	   {
		   mModel.removeLink(pRssFolder, pLink);
		   control("ListAllFeed");
	   }
	   public void removeRssFolder(RssFolder pRssFolder)
	   {
		   mModel.removeRssFolder(pRssFolder);
		   control("ListAllFeed");
	   }
	   public ArrayList<RssMessage> getRssMessage()
	   {
		   return mModel.getRssMessage();
	   }
	   public void addRssLink(RssFolder pRssFolder, RssLink pRssLink)
	   {
		   mModel.addRssLink(pRssFolder, pRssLink);
		   control("ListAllFeed");
	   }
	   public String connect(String pLogin, String pPassword)
	   {
		   String lResponse = null;
       		try {
	       		if (pLogin != "" && pPassword!= "")
	       			lResponse = mUrl.sendGet(ApiUrls.RSS_LOGIN, pLogin + ":" + pPassword);
	       		else
	       			return "Login or password not fill";
	       	} catch (Exception e1) {
					// TODO Auto-generated catch bloc			
					return ("Check Internet Connection!!!");
				}
	       	if (lResponse != null)
	       	{
	       		
	       		String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					setDataUser(pLogin, pPassword);
		    		setUser(JsonParser.parseLogin(lResponse));
		    		if (getDataUser() != null)
						try {
							lResponse = mUrl.sendGet(ApiUrls.RSS_ADD_FOLDER, getHeader());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		    		ArrayList<RssFolder> lRssFolder = JsonParser.parseUserFolders(lResponse);
					setRssFolder(lRssFolder);
					return "";
				}	
	       		return "Can't Login";
	       	}
       	return "Login or password incorrect";
	   }

	   public void deleteRssFolderOrLink(String pSelectedItem)
		{
			int i = 0;
			String lResponse = null;
			for (RssFolder lFolder : getRssFolder()) {
				if (lFolder.getTitle() == pSelectedItem) {
					try {
						lResponse = mUrl.sendDelete(ApiUrls.getDeleteFolderUrl(lFolder.getId()), getHeader());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (lResponse != null)
					{
						String delims = "[ {:,]+";
						String[] tokens = lResponse.split(delims);
						if (tokens[2].contentEquals("\"OK\""))
						{
							removeRssFolder(getRssFolder().get(i));
							control("All");
						}					
					}
					break ;
				}
				for (RssLink lLink : lFolder.getLinks()) {
					if (lLink.getTitle() == pSelectedItem) {
						try {
							 lResponse = mUrl.sendDelete(ApiUrls.getDeleteFeedUrl(lFolder.getId(), lLink.getFeedId()), getHeader());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (lResponse != null)
						{
							String delims = "[ {:,]+";
							String[] tokens = lResponse.split(delims);
							if (tokens[2].contentEquals("\"OK\""))
							{
								removeLink(getRssFolder().get(i), lLink);
								control("All");
							}
							
						}
						return ;
					}
				}
				++i;
			}
		}
	   public RssLink getSelectedLink(String pName)
	   {
		   for (RssFolder lFolder : getRssFolder()) {			
				for (RssLink lLink : lFolder.getLinks()) {					
					if (pName.contentEquals(lLink.getTitle()))
						return lLink;					
				}
			}
		   return null;
	   }
	   public String getNameItemTree(String pName)
	   {
		   String delims = "[{}]+";
		   String[] tokens = pName.split(delims);
		   return tokens[1];
	   }
	   public void showFeed(String pName)
	   {
		   String lName = getNameItemTree(pName);
		   RssLink lLink = getSelectedLink(lName);
		   if (lLink != null)	{
			   String lResponse = null;
			   try {
				lResponse = mUrl.sendGet(ApiUrls.getFeedsByFeedId(lLink.getFeedId()), getHeader());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			   if (lResponse != null)
			   {
				   ArrayList<RssMessage> lRssMessage = JsonParser.parseFeedsData(lResponse);
				   setRssMessage(lRssMessage);
			   }
		   }
	   }
	   public RssMessage getSelectedFeed(String pName) {
		   if (getRssMessage() != null)
			   for (RssMessage lFeed : getRssMessage()) {
				   if (pName.contentEquals(lFeed.getTitle()))
					   return lFeed;
			   }
		   return null;
	   }
	   public void displayFeed(String pName) {
		   String lName = getNameItemTree(pName); 
		   control(lName);
	   }
	   public void getListFolder()
	   {
		   String lResponse = "";
			try {
				if (getDataUser() != null)
					lResponse = mUrl.sendGet(ApiUrls.RSS_FOLDERS, getHeader());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.err.println("No internet connection");
				lResponse = "";
			}
			if (lResponse != "") {
				ArrayList<RssFolder> lRssFolder = JsonParser.parseUserFolders(lResponse);
				setRssFolder(lRssFolder);
			}
	   }
	   public RssFolder getRssFolderItem(String pName)
	   {
		   for (RssFolder lRssFolder : getRssFolder())
			   if (lRssFolder.getTitle().contentEquals(pName))
				   return lRssFolder;
		   return null;
	   }
	   public String addFeed(String pSelectedItem, String pName, String pUrl)
	   {
		   if (pSelectedItem == "")
			   return "No selected folder";
		   if (pName == "" || pUrl == "")
			   return "Name or Url empty";
		   String lResponse = "";
		   RssFolder lRssFolder = getRssFolderItem(pSelectedItem);
		   if (lRssFolder == null)
			   return "Folder not found";
		   try {
			   String lArg =createArgString(pName, pUrl);
			   lResponse = mUrl.sendPost(ApiUrls.getAddFeedFolder(lRssFolder.getId()), getHeader(), lArg);
		   	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   }
		   if (lResponse != "")
		   {
			   String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					RssLink lRssLink = JsonParser.parseNewlyCreatedFeed(lResponse);
					if (lRssLink == null)
						return "Connection failed";
					addRssLink(lRssFolder, lRssLink);
					return "";
					
				}
				return "Already exist";
		   }
		   return "Error Unknown";
	   }
	   public String addFolder(String pName)
	   {
		   String lResponse = "";
		   if (pName == "")
			   return "Empty field";
		   try {
			lResponse = mUrl.sendPost(ApiUrls.RSS_ADD_FOLDER, getHeader(), "name="+ pName);
		   	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   }
		   if (lResponse != "")
		   {
			   String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					RssFolder lRssFolder = JsonParser.parseNewlyCreatedFolder(lResponse);
					if (lRssFolder == null)
						return "Connection failed";
					addRssFolder(lRssFolder);
					return "";
					
				}
				 return "Name already exist";
		   }
		   return "Error unknown";
	   }
	   public String createArgString(String pName, String pUrl)
	   {
		   String lArg = "";
			if (pName != "")
				lArg += "name=" + pName;
			if (pUrl != "" && pName != "")
				lArg += "&url=" + pUrl;
			else if (pUrl != "" && pName == "")
				lArg = "url=" + pUrl;
			return lArg;
	   }
	   public String renameFolderOrLink(String pSelectedItem, String pName, String pUrl)
	   {
		   if (pName == "")
			   return "Empty field Name";
			String lResponse = null;
			for (RssFolder lFolder : getRssFolder()) {
				if (lFolder.getTitle() == pSelectedItem) {
					try {
						lResponse = mUrl.sendPut(ApiUrls.getUpdateFolderUrl(lFolder.getId()), getHeader(), "name="+ pName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (lResponse != null)
					{
						String delims = "[ {:,]+";
						String[] tokens = lResponse.split(delims);
						if (tokens[2].contentEquals("\"OK\""))
						{
							lFolder.setTitle(pName);
							control("ListAllFeeds");
							return "";
						}	
						return "Name Already used";
					}
					break ;
				}
				for (RssLink lLink : lFolder.getLinks()) {
					if (lLink.getTitle() == pSelectedItem) {
						try {
							String lArg =createArgString(pName, pUrl);
							 lResponse = mUrl.sendPut(ApiUrls.getUpdateFeedUrl(lLink.getFeedId()), getHeader(), lArg);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (lResponse != null)
						{
							String delims = "[ {:,]+";
							String[] tokens = lResponse.split(delims);
							if (tokens[2].contentEquals("\"OK\""))
							{
								if (pName != "")
									lLink.setTitle(pName);
								if (pUrl != "")
									lLink.setUrl(pUrl);
								control("ListAllFeeds");
								return "";
							}
							return "Url or Name incorrect";
						}
					}
				}
			}
			return "Unknown Error";
	   }
	   public String createArgStringUpdateUser(String pLogin, String pPassword)
	   {
		   String lArg = "";
			if (pLogin != "")
				lArg += "username=" + pLogin;
			if (pPassword != "" && pLogin != "")
				lArg += "&password=" + pPassword;
			else if (pPassword != "" && pLogin == "")
				lArg = "password=" + pPassword;
			return lArg;
	   }
	   public String updateInfoUser(String pLogin, String pPassword)
	   {
		   if (pLogin == "" && pPassword == "")
			   return "Login or Password empty";
		   String lResponse = "";
		   try {
			   String lArg = createArgStringUpdateUser(pLogin, pPassword);
			   lResponse = mUrl.sendPut(ApiUrls.RSS_UPDATE_ACC, getHeader(), lArg);
		   	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   }
		   if (lResponse != "")
		   {
			   	String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					System.out.println("LOGIN " + getUser().getLogin() + " PASSWORD " + getUser().getPassword());
					setUser(JsonParser.parseLogin(lResponse));
					if (pLogin == "")
						setDataUser(getUser().getLogin(), pPassword);
					else if (pPassword == "")
						setDataUser(pLogin, getUser().getPassword());
					else
						setDataUser(getUser().getLogin(), getUser().getPassword());
					return "";					
				}
				return "Already exist";
		   }
		   return "Error Unknown";
	   }
	   public String recupPassword(String pEmail)
	   {
		   if (pEmail == "")
			   return "Email empty";
		   String lResponse = "";
		   try {
			   lResponse = mUrl.sendPost(ApiUrls.FORGOTTEN_PASSWORD, getHeader(), "email="+ pEmail);
		   	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   }
		   if (lResponse != "")
		   {
			   	String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					return "";					
				}
				return "Error Unknown";
		   }
		   return "Error Unknown";
	   }
	   public String createAccount(String pEmail, String pUserName, String pPassword)
	   {
		   if (pEmail == "" || pUserName == "" || pPassword == "")
			   return "Email, Username or Password empty";
		   String lResponse = "";
		   try {
			   lResponse = mUrl.sendPost(ApiUrls.RSS_SIGNIN, getHeader(), "email="+ pEmail + "&username=" + pUserName +"&password=" + pPassword);
		   	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		   }
		   if (lResponse != "")
		   {
			   String delims = "[ {:,]+";
				String[] tokens = lResponse.split(delims);
				if (tokens[2].contentEquals("\"OK\""))
				{
					return "";					
				}
				return "Invalid email";
		   }
		   return "Error Unknown";
	   }
  abstract void control(String pViewName);
}