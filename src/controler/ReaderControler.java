package controler;

import model.AbstractModel;

public class ReaderControler extends AbstractControler {
  public ReaderControler(AbstractModel pModel) {
    super(pModel);
  }

  public void control(String pViewName) {
    //On notifie le modele d'une action si le controle est bon
    //--------------------------------------------------------    
     this.mModel.connect(pViewName);
  }
}