package Requests;

public final class ApiUrls {

	    public static final String BASE_URL = "http://rssapi.magrin.fr";

	    public static final String RSS_LOGIN = BASE_URL + "/user/login";
	    public static final String RSS_SIGNIN = BASE_URL + "/user";
	    public static final String RSS_FOLDERS = BASE_URL + "/folder";
	    public static final String FORGOTTEN_PASSWORD = BASE_URL + "/user/forgot";
	    public static final String RSS_UPDATE_FEED = BASE_URL + "/feed/";
	    public static final String RSS_UPDATE_FOLDER = BASE_URL + "/folder/";
	    public static final String RSS_ADD_FOLDER = BASE_URL + "/folder";
	    public static final String RSS_ADD_FEED = BASE_URL + "/feed/";
	    public static final String RSS_LOGOUT = BASE_URL + "/user/logout";
	    public static final String RSS_UPDATE_ACC = BASE_URL + "/user";
	    public static final String RSS_GET_FEED = BASE_URL + "/feed";

	    public static String getDeleteFeedUrl(String folderId, String feedId) {
	        return RSS_UPDATE_FEED + folderId + "/" + feedId;
	    }

	    public static String getUpdateFeedUrl(String feedId) {
	        return RSS_UPDATE_FEED + feedId;
	    }

	    public static String getUpdateFolderUrl(String folderId) {
	        return RSS_UPDATE_FOLDER + folderId;
	    }

	    public static String getDeleteFolderUrl(String folderId) {
	        return RSS_UPDATE_FOLDER + folderId;
	    }

	    public  static String getAddFeedFolder(String folderId) {
	        return RSS_ADD_FEED + folderId;
	    }

	    public static String getFeedsByFeedId(String feedId) {
	        return RSS_GET_FEED + "/" + feedId;
	    }
	}