package Requests;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import javax.xml.bind.DatatypeConverter;
 
public class MyHttpURLConnection {
 
	private final String USER_AGENT = "Mozilla/5.0";

	// HTTP GET request
	public String sendGet(String pUrl, String pHeader) throws Exception {
 
		URL obj = new URL(pUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		// optional default is GET
		con.setRequestMethod("GET");
       
		//add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
    	con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
    	
    	// Prepare Header
    	if (pHeader != "") {
    		String encodeForHeader = DatatypeConverter.printBase64Binary(pHeader.getBytes("UTF-8"));
    		con.setRequestProperty("Authorization", "Basic " + encodeForHeader);
    	}
        int responseCode = 0;
        try {
        	responseCode = con.getResponseCode();
        }	catch (UnknownHostException e) {           
           throw new Exception();
        } catch (MalformedURLException e) {         
          System.out.println("Invalid URL");          
          return "";
        } catch (IOException e) {
            System.out.println("Error reading URL");
            return "";
        } catch (Exception e) {
            return "";
        }
		System.out.println("\nSending 'GET' request to URL : " + pUrl);
		System.out.println("Response Code : " + responseCode);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
		return (response.toString());
	}
 
	// HTTP POST request
	public String sendPost(String pUrl, String pHeader, String pUrlParameters) throws Exception {
		URL obj = new URL(pUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		
		  // Prepare Header
			if (pHeader != "") {
				String encodeForHeader = DatatypeConverter.printBase64Binary(pHeader.getBytes("UTF-8"));
				con.setRequestProperty("Authorization", "Basic " + encodeForHeader);
			}
			
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = null;
		try {
			wr = new DataOutputStream(con.getOutputStream());
		} catch ( IOException e) {
			System.out.println("NO CONNECTION");
			return "";
		}
		
		wr.writeBytes(pUrlParameters);
		wr.flush();
		wr.close();
		
		int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (UnknownHostException e) {	           
			throw new Exception();
		} catch (MalformedURLException e) {         
	          System.out.println("Invalid URL");          
	          return "";
	        } catch (IOException e) {
	            System.out.println("Error reading URL");
	            return "";
	        } catch (Exception e) {
	            return "";
	        }
		System.out.println("\nSending 'POST' request to URL : " + pUrl);
		System.out.println("Post parameters : " + pUrlParameters);
		System.out.println("Response Code : " + responseCode);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
		return (response.toString());
	}
 
	public String sendDelete(String pUrl, String pHeader) throws Exception
	{
		URL url = null;
		try {
		    url = new URL(pUrl);
		} catch (MalformedURLException exception) {
		    exception.printStackTrace();
		}
		HttpURLConnection httpURLConnection = null;
		try {
		    httpURLConnection = (HttpURLConnection) url.openConnection();
		    httpURLConnection.setRequestProperty("Content-Type",
		                "application/x-www-form-urlencoded");
		    // Prepare Header
			if (pHeader != "") {
				String encodeForHeader = DatatypeConverter.printBase64Binary(pHeader.getBytes("UTF-8"));
				httpURLConnection.setRequestProperty("Authorization", "Basic " + encodeForHeader);
			}
		    httpURLConnection.setRequestMethod("DELETE");
			try {
				httpURLConnection.getResponseCode();
			} catch (UnknownHostException e) {	           
				throw new Exception();
			} 
			catch (MalformedURLException e) {         
		          System.out.println("Invalid URL");          
		          return "";
		        } catch (IOException e) {
		            System.out.println("Error reading URL");
		            return "";
		        } catch (Exception e) {
		            return "";
		        }
		} catch (IOException exception) {
		    exception.printStackTrace();
		} finally {         
		    if (httpURLConnection != null) {
		    	BufferedReader in = null;
		    	in = new BufferedReader(
		    			new InputStreamReader(httpURLConnection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();	 
					while ((inputLine = in.readLine()) != null) {
						response.append(inputLine);
					}
					in.close();
				//print result
				return (response.toString());
//		        httpURLConnection.disconnect();
		    }
		}
		return null; 
	}
	
	public String sendPut(String pUrl, String pHeader, String pData)throws Exception {
		URL obj = new URL(pUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add request header
		con.setRequestMethod("PUT");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		// Prepare Header
		if (pHeader != "") {
			String encodeForHeader = DatatypeConverter.printBase64Binary(pHeader.getBytes("UTF-8"));
			con.setRequestProperty("Authorization", "Basic " + encodeForHeader);
		}
			
		
		// Send put request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(pData);
		wr.flush();
		wr.close();
 
		int responseCode = 0;
		try {
			responseCode = con.getResponseCode();
		} catch (UnknownHostException e) {	           
			throw new Exception();
		} catch (MalformedURLException e) {         
	          System.out.println("Invalid URL");          
	          return "";
	        } catch (IOException e) {
	            System.out.println("Error reading URL");
	            return "";
	        } catch (Exception e) {
	            return "";
	        }
		System.out.println("\nSending 'PUT' request to URL : " + pUrl);
		System.out.println("Post parameters : " + pData);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
 
		//print result
		System.out.println(response.toString());
		return (response.toString());
	}
}