package oberver;

public interface Observer {
	  public void update(String pViewName);
}