package parser;

import model.RssFolder;
import model.RssLink;
import model.RssMessage;
import model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ROMMM on 09/10/14.
 */
public class JsonParser {

    static private String MESSAGES = "messages";
    static private String TITLE = "title";
    static private String LINK = "link";
    static private String AUTHOR = "author";
    static private String DATE = "publishedDate";
    static private String CONTENTSNIPPET = "contentSnippet";
    static private String CONTENT = "content";
    static private String CATEGORIES = "categories";

    static private Pattern p = Pattern.compile("<img src=\"(\\S+)\">");
    static private Matcher m;

    static public ArrayList<RssMessage> parseFeedsData(String response) {

        ArrayList<RssMessage> messagesList = new ArrayList<RssMessage>();

        if (!getRequestResult(response))
            return null;

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray(MESSAGES);

            for (int i = 0 ; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);

                RssMessage message = new RssMessage();

                message.setTitle(convertFromUTF8(object.getString(TITLE)));
                message.setLink(convertFromUTF8(object.getString(LINK)));
                message.setAuthor(convertFromUTF8(object.getString(AUTHOR)));
                message.setDate(object.getString(DATE));
                message.setContentSnippet(convertFromUTF8(object.getString(CONTENTSNIPPET)));
                message.setContent(convertFromUTF8(object.getString(CONTENT)));

                JSONArray categories = object.getJSONArray(CATEGORIES);

                m = p.matcher(message.getContent());

                while (m.find())
                    message.getImgUrls().add(m.group(1));

                for (int j = 0; categories != null && j < categories.length(); j++) {
                        message.getCategories().add(categories.getString(j));
                }

                messagesList.add(message);
            }

            return (messagesList);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static User parseLogin(String response) {

        try {
            JSONObject object = new JSONObject(response);

            if (object.getString("status").compareTo("OK") != 0)
                return null;

            User user = new User();

            object = object.getJSONObject("message");
            user.setLogin(convertFromUTF8(object.getString("username")));
            user.setEmail(convertFromUTF8(object.getString("email")));
            user.setId(object.getString("_id"));

            return user;

        } catch (JSONException e) {
            return null;
        }
    }

    public static ArrayList<RssFolder> parseUserFolders(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("message");

            ArrayList<RssFolder> folders = new ArrayList<RssFolder>();
            ArrayList<RssLink> links;

            for (int i = 0; i < jsonArray.length(); i++) {

                RssFolder folder = new RssFolder();
                links = new ArrayList<RssLink>();

                folder.setTitle(convertFromUTF8(jsonArray.getJSONObject(i).getString("name")));
                folder.setId(jsonArray.getJSONObject(i).getString("_id"));

                JSONArray array = jsonArray.getJSONObject(i).getJSONArray("_feeds");

                for (int j = 0; j < array.length(); j++) {
                    RssLink link = new RssLink();

                    link.setFeedId(array.getJSONObject(j).getString("_id"));
                    link.setTitle(convertFromUTF8(array.getJSONObject(j).getString("name")));
                    link.setUrl(convertFromUTF8(array.getJSONObject(j).getString("url")));
                    link.setIcon(array.getJSONObject(j).getString("icon"));
                    links.add(link);
                }
                folder.setLinks(links);
                folders.add(folder);
            }

            return folders;

        } catch (JSONException ex) {

            ex.printStackTrace();
            return null;
        }
    }

    public static boolean getRequestResult(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);

            if (jsonObject.getString("status").compareTo("OK") != 0)
                return false;

            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getRequestMessage(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.getString("message");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static RssFolder parseNewlyCreatedFolder(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.getString("status").compareTo("OK") != 0)
                return null;

            jsonObject = jsonObject.getJSONObject("message");
            RssFolder folder = new RssFolder();

            folder.setTitle(jsonObject.getString("name"));
            folder.setId(jsonObject.getString("_id"));
            folder.setLinks(new ArrayList<RssLink>());
            System.out.println("NEW FOLDER");
            return folder;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static RssLink parseNewlyCreatedFeed(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.getString("status").compareTo("OK") != 0)
                return null;

            jsonObject = jsonObject.getJSONObject("message");
            RssLink link = new RssLink();

            link.setTitle(jsonObject.getString("name"));
            link.setFeedId(jsonObject.getString("_id"));
            link.setUrl(jsonObject.getString("url"));
            return link;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static String convertFromUTF8(String s) {
		  String out = null;
		  try {
		    out = new String(s.getBytes(), "UTF-8");
		  } catch (java.io.UnsupportedEncodingException e) {
		    return null;
		  }
		  return out;
		}
}